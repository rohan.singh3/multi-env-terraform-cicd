# unset errexit option as we want to HANDLE the 'terraform fmt' error with proper error messages
# instead of simply exiting upon facing an error
set +o errexit

# check terraform format
terraform fmt -recursive -check -diff
if [ ! $? -eq 0 ]; then
  echo "FAILED terraform fmt."
  exit 9
fi

# set back errexit option
set -o errexit
