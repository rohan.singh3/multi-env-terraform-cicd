# exit script upon any non-zero exit code
set -o errexit

source ./scripts/terraform_fmt.sh

# prepare tf-executor SA key
mkdir -p ./tmp/
PATH_TO_DECODED_SA_KEY="${PWD}/tmp/gcloud-sa-${ENV}.json"
echo $ENCODED_TF_EXECUTOR_SA_KEY | base64 -d > $PATH_TO_DECODED_SA_KEY
export GOOGLE_CREDENTIALS=$PATH_TO_DECODED_SA_KEY # see: https://www.terraform.io/docs/backends/types/gcs.html#configuration-variables

# prepare tf-owner SA email
DECODED_TF_OWNER_EMAIL=$(echo $ENCODED_TF_OWNER_SA_EMAIL | base64 -d)
export TF_VAR_terraform_owner_email=$DECODED_TF_OWNER_EMAIL # TF_VAR_ will be used by terraform modules' variables

# initialize terraform
cd envs/${ENV}
terraform get
terraform init
