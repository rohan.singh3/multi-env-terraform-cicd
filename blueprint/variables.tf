## --- REQUIRED PARAMETERS ------------------------------------------------------------------------------------------------

variable "tf_env" {
  description = "Just an identifier that will be used in GCP resource names. Will help us distinguish resources created by Terraform versus resources that were already created before Terraform"
  type        = string
}

variable "terraform_owner_email" {
  description = "Email address of the ServiceAccount that can be impersonated to perform TF operations"
  type        = string
}

variable "gcp_project_id" {
  type        = string
  description = "Id of the GCP project"
}

variable "region" {
  type        = string
  description = "Region where the GCE VM Instance resides. Defaults to the Google provider's region if nothing is specified here. See https://cloud.google.com/compute/docs/regions-zones"
}

## -- OPTIONAL PARAMETERS -------------------------------------------------------------------
variable "vpc_name" {
  type        = string
  description = "GCP VPC Name in which GCE VM will be created"
  default     = "default"
}