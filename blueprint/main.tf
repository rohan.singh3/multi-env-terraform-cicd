resource "random_string" "launch_id" {
  length  = 4
  special = false
  upper   = false
}

locals {
  suffix = format("%s-%s", var.tf_env, random_string.launch_id.result)
}

// module "gce_instance" {
//   source           = "../modules/gce"
//   suffix           = local.suffix
//   gcp_project_id   = var.gcp_project_id
//   vpc_network_name = var.vpc_name
//   instance_name    = "kylo-ren"
//   network_tags     = ["http-server", "https-server"]
// }
