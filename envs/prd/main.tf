terraform {
  backend "gcs" {
    bucket = "multi-env-prd-tfstate"
    prefix = "main"
  }
}

variable "terraform_owner_email" {
  description = "Email address of the ServiceAccount that has permissions to perform Terraform operations"
  type        = string
  # value should be obtained from environment variables only
}

module "dev_infra" {
  source                = "../../blueprint"
  terraform_owner_email = var.terraform_owner_email
  gcp_project_id        = "searce-playground"
  tf_env                = "tfprd"
  region                = "asia-southeast1"
  vpc_name              = "terraform-network"
}
